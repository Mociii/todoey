import 'package:flutter/material.dart';

class ArchivedTaskCard extends StatefulWidget {
  final String title;
  final String subTitle;
  final bool isDone;
  final int daysLeft;
  final String initialDate;
  final String endDate;
  final String doneDate;
  final Function deleteCallBack;

  ArchivedTaskCard({
    this.title,
    this.subTitle,
    this.isDone,
    this.daysLeft,
    this.initialDate,
    this.endDate,
    this.doneDate,
    this.deleteCallBack,
  });

  @override
  _ArchivedTaskCardState createState() => _ArchivedTaskCardState();
}

class _ArchivedTaskCardState extends State<ArchivedTaskCard> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        child: Column(
          children: <Widget>[
            _archivedTaskListTile(),
            _expanded ? _additionalInformationWidget() : Container(),
          ],
        ),
      ),
      onLongPress: () => widget.deleteCallBack(),
    );
  }

  Widget _archivedTaskListTile() {
    return ListTile(
      title: Text(widget.title),
      subtitle: Text(
        '${widget.subTitle != '' ? widget.subTitle : 'Task Has No Description'}',
      ),
      trailing: IconButton(
        icon: _expanded
            ? const Icon(Icons.expand_less)
            : const Icon(Icons.expand_more),
        onPressed: () => setState(() => _expanded = !_expanded),
      ),
    );
  }

  Widget _additionalInformationWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 20),
      width: double.infinity,
      child: Text(
        'Task Is Done: ${widget.isDone ? 'Yes' : 'No'}\n\n'
        'Initial Date: ${widget.initialDate != null ? widget.initialDate : 'non'}\n\n'
        'Expiration Date: ${widget.endDate != null ? widget.endDate : 'non'}\n\n'
        'Days Left: ${widget.daysLeft != null ? widget.daysLeft : 'non'}\n\n'
        'Done Date: ${widget.doneDate != null ? widget.doneDate : 'non'}\n',
      ),
    );
  }
}
