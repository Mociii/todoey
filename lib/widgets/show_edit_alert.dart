import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/models/task.dart';
import 'package:todoey_flutter/providers/tasks.dart';

class ShowEditAlert extends StatefulWidget {
  final Task _task;

  ShowEditAlert(this._task);

  @override
  _ShowEditAlertState createState() => _ShowEditAlertState();
}

class _ShowEditAlertState extends State<ShowEditAlert> {
  bool _wantToEdit = false;
  String _errorText;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Edit Task'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _contentText(),
          const SizedBox(height: 40),
          _wantToEdit
              ? _editTextField()
              : Row(
                  children: <Widget>[
                    _alertButtons(context, Icons.check, _checkOnTap),
                    _alertButtons(context, Icons.cancel, _cancelOnTap),
                  ],
                ),
        ],
      ),
    );
  }

  Widget _contentText() {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          const TextSpan(
            text: 'Are you sure you want to edit task ',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
          TextSpan(
            text: "'${widget._task.title}'",
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          const TextSpan(
            text: ' ?',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
        ],
      ),
    );
  }

  Widget _editTextField() {
    return Column(
      children: <Widget>[
        TextField(
          decoration: InputDecoration(
            hintText: 'Enter New Title',
            errorText: _errorText,
          ),
          autofocus: true,
          onSubmitted: _editTextFieldOnSubmitted,
        ),
        const SizedBox(height: 25),
        RaisedButton(
          child: const Text('Cancel'),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ],
    );
  }

  Widget _alertButtons(BuildContext context, IconData icon, Function onTap) {
    return Expanded(
      child: InkWell(
        splashColor: Colors.grey,
        child: Container(
          height: 50,
          alignment: Alignment.center,
          child: Icon(
            icon,
            color: icon == Icons.check ? Colors.green : Colors.red,
          ),
        ),
        onTap: onTap,
      ),
    );
  }

  void _editTextFieldOnSubmitted(String newTaskTitle) {
    if (newTaskTitle == '') {
      setState(() => _errorText = 'Enter new Title!');
    } else {
      Provider.of<Tasks>(context, listen: false)
          .updateTaskTitle(widget._task, newTaskTitle);
      Navigator.of(context).pop();
    }
  }

  void _checkOnTap() {
    print('********YES TAPPED********');
    setState(() => _wantToEdit = true);
  }

  void _cancelOnTap() {
    print('********No TAPPED********');
    Navigator.of(context).pop();
  }
}
