import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/widgets/main_task_tile.dart';
import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/widgets/show_archive_alert.dart';
import 'package:todoey_flutter/widgets/show_edit_alert.dart';

class MainTasksList extends StatelessWidget {
  final ScrollController _controller;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  MainTasksList(this._controller, this._scaffoldKey);

  @override
  Widget build(BuildContext context) {
    return Consumer<Tasks>(
      builder: (context, tasks, child) => ListView.builder(
        controller: _controller,
        itemBuilder: (ctx, index) {
          return MainTaskTile(
            title: tasks.tasksData[index].title,
            subTitle: tasks.tasksData[index].subTitle,
            isDone: tasks.tasksData[index].isDone,
            daysLeft: tasks.tasksData[index].daysLeft,
            endDate: tasks.tasksData[index].endDate,
            checkCallBack: (newVal) =>
                tasks.updateTaskIsDone(tasks.tasksData[index]),
            deleteCallBack: () => showDialogs(context,
                ShowArchiveAlert(tasks.tasksData[index], _scaffoldKey)),
            editCallBack: () =>
                showDialogs(context, ShowEditAlert(tasks.tasksData[index])),
          );
        },
        itemCount: tasks.tasksLength,
      ),
    );
  }

  Future<void> showDialogs(BuildContext context, Widget widget) async {
    await showDialog(
      context: context,
      builder: (ctx) => widget,
    );
  }
}
