import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/providers/tasks.dart';

class ShowAddAlert extends StatelessWidget {
  final String _taskTitle;
  final String _taskSubTitle;

  ShowAddAlert(this._taskTitle, this._taskSubTitle);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Task Already Exists'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _contentText(),
          const SizedBox(height: 40),
          Row(
            children: <Widget>[
              _alertButtons(context, Icons.check, _checkOnTap),
              _alertButtons(context, Icons.cancel, _cancelOnTap),
            ],
          ),
        ],
      ),
    );
  }

  Widget _contentText() {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          const TextSpan(
            text: 'This task with title ',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
          TextSpan(
            text: "'$_taskTitle'",
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          const TextSpan(
            text: ' already exists.do you want to add it again ?',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
        ],
      ),
    );
  }

  Widget _alertButtons(BuildContext context, IconData icon, Function onTap) {
    return Expanded(
      child: InkWell(
        splashColor: Colors.grey,
        child: Container(
          height: 50,
          alignment: Alignment.center,
          child: Icon(
            icon,
            color: icon == Icons.check ? Colors.green : Colors.red,
          ),
        ),
        onTap: () => onTap(context),
      ),
    );
  }

  void _checkOnTap(BuildContext context) {
    print('********YES TAPPED********');
    Provider.of<Tasks>(context, listen: false)
        .addTask(_taskTitle, _taskSubTitle);
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    _setStateOff(context);
  }

  void _cancelOnTap(BuildContext context) {
    print('********No TAPPED********');
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    _setStateOff(context);
  }

  void _setStateOff(BuildContext context) {
    Provider.of<Tasks>(context, listen: false).changeIsDateIncluded(false);
    Provider.of<Tasks>(context, listen: false).changeIsSubtitleIncluded(false);
    Provider.of<Tasks>(context, listen: false).changeSelectedDate(null);
  }
}
