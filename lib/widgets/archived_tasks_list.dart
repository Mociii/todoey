import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/widgets/archived_task_card.dart';
import 'package:todoey_flutter/widgets/show_delete_alert.dart';

class ArchivedTasksList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<Tasks>(
      builder: (context, tasks, child) => ListView.builder(
        itemBuilder: (context, index) => ArchivedTaskCard(
          title: tasks.archivedTasks[index].title,
          subTitle: tasks.archivedTasks[index].subTitle,
          isDone: tasks.archivedTasks[index].isDone,
          daysLeft: tasks.archivedTasks[index].daysLeft,
          initialDate: tasks.archivedTasks[index].initialDate,
          endDate: tasks.archivedTasks[index].endDate,
          doneDate: tasks.archivedTasks[index].doneDate,
          deleteCallBack: () => showDialog(
            context: context,
            builder: (context) => ShowDeleteAlert(tasks.archivedTasks[index]),
          ),
        ),
        itemCount: tasks.archivedTasksLength,
      ),
    );
  }
}
