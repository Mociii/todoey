import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/models/task.dart';
import 'package:todoey_flutter/providers/tasks.dart';

class ShowArchiveAlert extends StatelessWidget {
  final Task _task;
  final GlobalKey<ScaffoldState> _scaffoldKey;

  ShowArchiveAlert(this._task, this._scaffoldKey);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Archive Task'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _contentText(),
          const SizedBox(height: 40),
          Row(
            children: <Widget>[
              _alertButtons(context, Icons.check, _checkOnTap),
              _alertButtons(context, Icons.cancel, _cancelOnTap),
            ],
          ),
        ],
      ),
    );
  }

  Widget _contentText() {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          const TextSpan(
            text: 'Are you sure you want to archive task ',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
          TextSpan(
            text: "'${_task.title}'",
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          const TextSpan(
            text: ' ?',
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
        ],
      ),
    );
  }

  Widget _alertButtons(BuildContext context, IconData icon, Function onTap) {
    return Expanded(
      child: InkWell(
        splashColor: Colors.grey,
        child: Container(
          height: 50,
          alignment: Alignment.center,
          child: Icon(
            icon,
            color: icon == Icons.check ? Colors.green : Colors.red,
          ),
        ),
        onTap: () => onTap(context),
      ),
    );
  }

  void _checkOnTap(BuildContext context) {
    print('********YES TAPPED********');
    Provider.of<Tasks>(context, listen: false).archiveTask(_task);
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text('Task ${_task.title} archived.'),
        duration: const Duration(seconds: 1),
        backgroundColor: Colors.lightBlueAccent,
      ),
    );
    Navigator.of(context).pop();
  }

  void _cancelOnTap(BuildContext context) {
    print('********No TAPPED********');
    Navigator.of(context).pop();
  }
}
