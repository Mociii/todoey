import 'package:flutter/material.dart';

class MainTaskTile extends StatelessWidget {
  final String title;
  final String subTitle;
  final bool isDone;
  final int daysLeft;
  final String endDate;
  final Function checkCallBack;
  final Function deleteCallBack;
  final Function editCallBack;

  MainTaskTile({
    this.title,
    this.subTitle,
    this.isDone,
    this.daysLeft,
    this.endDate,
    this.checkCallBack,
    this.deleteCallBack,
    this.editCallBack,
  });

  @override
  Widget build(BuildContext context) {
    String _nextLine = '\n';
    return Dismissible(
      key: UniqueKey(),
      confirmDismiss: (direction) => _confirmDismissFunction(direction),
      background:
          _dismissibleBackGround(Alignment.centerLeft, 20, 0, Icons.edit),
      secondaryBackground:
          _dismissibleBackGround(Alignment.centerRight, 0, 20, Icons.archive),
      child: ListTile(
        title: Text(
          title,
          style:
              TextStyle(decoration: isDone ? TextDecoration.lineThrough : null),
//          textDirection: TextDirection.rtl,
        ),
        trailing: Checkbox(
          activeColor: Colors.grey,
          value: isDone,
          onChanged: checkCallBack,
        ),
        subtitle: (subTitle == '' && daysLeft == null)
            ? null
            : Text(
                '${subTitle != '' ? subTitle : _nextLine = ''}'
                '${endDate != null ? '${_nextLine}Expiration Date: $endDate\n$daysLeft days left' : ''}',
              ),
        contentPadding: const EdgeInsets.only(left: 15, right: 30),
      ),
    );
  }

  Future<bool> _confirmDismissFunction(DismissDirection direction) async {
    if (direction == DismissDirection.endToStart) {
      await deleteCallBack();
      return false;
    } else {
      editCallBack();
      return false;
    }
  }

  Widget _dismissibleBackGround(
      AlignmentGeometry alignment, double left, double right, IconData icon) {
    return Container(
      color: Colors.lightBlueAccent,
      alignment: alignment,
      padding: EdgeInsets.only(left: left, right: right),
      child: Icon(icon, color: Colors.white),
    );
  }
}
