import 'package:flutter/material.dart';

import 'package:todoey_flutter/screens/archived_tasks_screen.dart';
import 'package:todoey_flutter/enums/drawer_state.dart';

class AppDrawer extends StatelessWidget {
  final DrawerState _drawerState;

  AppDrawer(this._drawerState);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          _todoeyTextWidget(),
          const SizedBox(height: 20),
          _drawerState == DrawerState.Main
              ? _selectScreen(
                  context,
                  'Archived Tasks',
                  () => Navigator.of(context)
                      .pushReplacementNamed(ArchivedTasksScreen.screenName),
                )
              : _selectScreen(
                  context,
                  'Main Tasks',
                  () => Navigator.pushReplacementNamed(context, '/'),
                )
        ],
      ),
    );
  }

  Widget _todoeyTextWidget() {
    return Container(
      height: 120,
      width: double.infinity,
      alignment: Alignment.center,
      child: const Text(
        'Todoey',
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 35,
        ),
      ),
      color: Colors.lightBlueAccent,
    );
  }

  Widget _selectScreen(BuildContext context, String text, Function onTap) {
    return InkWell(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8),
        margin: const EdgeInsets.only(left: 20),
        child: Row(
          children: <Widget>[
            const Icon(Icons.archive, color: Colors.grey),
            const SizedBox(width: 8),
            Text(
              text,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
