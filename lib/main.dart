import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/screens/main_tasks_screen.dart';
import 'package:todoey_flutter/screens/archived_tasks_screen.dart';
//import 'package:todoey_flutter/screens/custom_paint_test.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<Tasks>(
      create: (context) => Tasks(),
      child: MyMaterialApp(),
    );
  }
}

class MyMaterialApp extends StatefulWidget {
  @override
  _MyMaterialAppState createState() => _MyMaterialAppState();
}

class _MyMaterialAppState extends State<MyMaterialApp> {
  @override
  void initState() {
    super.initState();
    Provider.of<Tasks>(context, listen: false).db.init().then((_) {
      Provider.of<Tasks>(context, listen: false).loadTasksOnStart();
      Provider.of<Tasks>(context, listen: false).loadArchivedTasksOnStart();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todoey App',
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => MainTasksScreen(), //  CustomPaintTest(),
        ArchivedTasksScreen.screenName: (context) => ArchivedTasksScreen(),
      },
    );
  }
}
