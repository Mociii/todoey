class Task {
  String title;
  final String subTitle;
  final int id;
  bool isDone;
  bool isArchived;
  int daysLeft;
  final String initialDate;
  final String endDate;
  String doneDate;

  Task({
    this.title,
    this.subTitle,
    this.id,
    this.isDone = false,
    this.isArchived = false,
    this.daysLeft,
    this.initialDate,
    this.endDate,
    this.doneDate,
  });

  Task.fromDb(Map<String, dynamic> record)
      : title = record['title'],
        subTitle = record['subTitle'],
        id = record['id'],
        isDone = record['isDone'] == 1,
        isArchived = record['isArchived'] == 1,
        daysLeft = record['daysLeft'],
        initialDate = record['initialDate'],
        endDate = record['endDate'],
        doneDate = record['doneDate'];

  Map<String, dynamic> toMapForDb() {
    return {
      'title': title,
      'subTitle': subTitle,
      'id': id,
      'isDone': isDone ? 1 : 0,
      'isArchived': isArchived ? 1 : 0,
      'daysLeft': daysLeft,
      'initialDate': initialDate,
      'endDate': endDate,
      'doneDate': doneDate,
    };
  }

  void toggleDone() {
    isDone = !isDone;
  }

  void toggleArchived() {
    isArchived = !isArchived;
  }

  void changeTitle(String newTitle) {
    title = newTitle;
  }

  void changeDaysLeft(int newValue) {
    daysLeft = newValue;
  }

  void calculateDaysLeft() {
    DateTime parsedEndDate = DateTime.parse(
      endDate.substring(6, 10) +
          endDate.substring(3, 5) +
          endDate.substring(0, 2),
    );
    int daysLeft = parsedEndDate
        .difference(DateTime(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().day,
        ))
        .inDays;
    if (daysLeft > 0) {
      this.daysLeft = daysLeft;
    }
    return;
  }

  String toString() {
    return 'Task{(task title is: $title), (task id is: $id), (task isDone is: $isDone)'; // 'Task{(task title is: $title), (task id is: $id), (task isDone is: $isDone), (task isArchived is: $isArchived), (task daysLeft is: $daysLeft), (task initialDate is: $initialDate), (task endDate is: $endDate), (task doneDate is: $doneDate)}';
  }
}
