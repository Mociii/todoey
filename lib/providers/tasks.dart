import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:todoey_flutter/models/task.dart';
import 'package:todoey_flutter/local/database.dart';

class Tasks with ChangeNotifier {
  final TasksDatabase _db = TasksDatabase();
  List<Task> _tasksData = [];
  List<Task> _archivedTasks = [];
  int _remainTasks = 0;
  int _doneTasks = 0;
  bool _isDateIncluded = false;
  bool _isSubTitleIncluded = false;
  DateTime _selectedDate;

  List<Task> get tasksData => UnmodifiableListView(_tasksData);

  List<Task> get archivedTasks => UnmodifiableListView(_archivedTasks);

  int get tasksLength => _tasksData.length;

  int get archivedTasksLength => _archivedTasks.length;

  int get remainTasks => _remainTasks;

  int get doneTasks => _doneTasks;

  TasksDatabase get db => _db;

  bool get isDateIncluded => _isDateIncluded;

  bool get isSubTitleIncluded => _isSubTitleIncluded;

  DateTime get selectedDate => _selectedDate;

  Future<void> loadTasksOnStart() async {
    _tasksData = await _db.newFetchItems();
    _calculateRemainTasks();
    _calculateDoneTasks();
    _calculateLeftDaysOnStart();
    notifyListeners();
  }

  Future<void> loadArchivedTasksOnStart() async {
    _archivedTasks = await _db.fetchItems();
    _archivedTasks =
        _archivedTasks.where((element) => element.isArchived).toList();
    notifyListeners();
  }

  Future<void> addTask(String taskTitle, String taskSubTitle) async {
    final Task task = Task(
      title: taskTitle,
      subTitle: taskSubTitle,
      daysLeft: _isDateIncluded ? calculateLeftDays() : null,
      initialDate: _isDateIncluded
          ? DateFormat('dd/MM/yyyy').format(DateTime.now())
          : null,
      endDate: _isDateIncluded
          ? DateFormat('dd/MM/yyyy').format(_selectedDate)
          : null,
    );
    _tasksData.add(task);
    _db.addItem(task);
    _tasksData = await _db
        .fetchItems(); // because tasks added to list has no id & id is generated by Database
    _tasksData = _tasksData.where((element) => !element.isArchived).toList();
    _calculateRemainTasks();
    _db.printDb();
//    _tasksData.forEach((element) => print(element));
    notifyListeners();
  }

  Future<void> updateTaskIsDone(Task task) async {
    task.toggleDone();
    _db.updateItem(task);
    if (task.isDone) {
      task.doneDate = DateFormat('dd/MM/yyyy').format(DateTime.now());
      _tasksData.remove(task);
      _tasksData.add(task);
    } else {
      task.doneDate = null;
      _tasksData = await _db.newFetchItems();
      notifyListeners();
    }
    _calculateDoneTasks();

    _db.printDb();
    notifyListeners();
  }

  void updateTaskTitle(Task task, String newTitle) {
    task.changeTitle(newTitle);
    _db.updateItem(task);
    notifyListeners();
  }

  void archiveTask(Task task) {
    task.toggleArchived();
    _tasksData.remove(task);
    _archivedTasks.add(task);
    _db.updateItem(task);
    _calculateDoneTasks();
    _db.printDb();
    notifyListeners();
  }

  void deleteTask(Task task) {
    _archivedTasks.remove(task);
    _db.printDb();
    db.deleteItem(task.id);
    _db.printDb();
    notifyListeners();
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day),
      firstDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day),
      lastDate: DateTime(2100),
    );
    if (picked == null) {
      _selectedDate = null;
      notifyListeners();
      return;
    }
    _selectedDate = picked;
//    showTimePicker(context: null, initialTime: null);
    notifyListeners();
  }

  void changeSelectedDate(DateTime newValue) {
    _selectedDate = newValue;
    notifyListeners();
  }

  int calculateLeftDays() {
    if (_selectedDate == null) {
      return null;
    }

    return _selectedDate
        .difference(DateTime(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().day,
        ))
        .inDays;
  }

  void changeIsDateIncluded(bool newValue) {
    _isDateIncluded = newValue;
    notifyListeners();
  }

  void changeIsSubtitleIncluded(bool newValue) {
    _isSubTitleIncluded = newValue;
    notifyListeners();
  }

  void _calculateRemainTasks() {
    List<Task> newList =
        _tasksData.where((element) => !element.isDone).toList();
    _remainTasks = newList.length;
    notifyListeners();
  }

  void _calculateDoneTasks() {
    List<Task> newList = _tasksData.where((element) => element.isDone).toList();
    _doneTasks = newList.length;
    _remainTasks = _tasksData.length - _doneTasks;
    notifyListeners();
  }

  void _calculateLeftDaysOnStart() {
    int counter = 0;
    if (_tasksData.isNotEmpty) {
      _tasksData.forEach((element) {
        if (element.endDate != null && !element.isDone) {
//          print(
//              "FOR ELEMENT NUMBER $counter********************_calculateLeftDaysOnStart executed!********************");
//          print(
//              "ELEMENT NUMBER $counter INITIAL DATE*******************${element.initialDate}*********************");
//          print(
//              "ELEMENT NUMBER $counter END DATE***********************${element.endDate}*****************");
//          print(
//              "ELEMENT NUMBER $counter DAYS LEFT BEFORE CALCULATION**********************${element.daysLeft}******************");
          element.calculateDaysLeft();
//          print(
//              "ELEMENT NUMBER $counter DAYS LEFT AFTER CALCULATION**********************${element.daysLeft}******************");
        } else {
//          print(
//              "FOR ELEMENT NUMBER $counter ********************_calculateLeftDaysOnStart NOT executed!********************");
          counter++;
          return;
        }
        counter++;
      });
      notifyListeners();
    }
  }
}
