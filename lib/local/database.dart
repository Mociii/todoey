import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:todoey_flutter/models/task.dart';

class TasksDatabase {
  Database database;

  Future<void> init() async {
    database = await openDatabase(
      join(await getDatabasesPath(), 'todoey.db'),
      version: 1,
      onCreate: (Database db, int version) {
        db.execute("""
          CREATE TABLE tasks
          (
            title TEXT,
            subTitle TEXT,
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            isDone INTEGER,
            isArchived INTEGER,
            daysLeft INTEGER,
            initialDate TEXT,
            endDate TEXT,
            doneDate TEXT
          )
        """);
      },
    );
  }

  void printDb() async {
    List<Map<String, dynamic>> list = await database.query('tasks');
    print(list);
  }

  Future<void> addItem(Task task) async {
    int number = await database.insert('tasks', task.toMapForDb());
    print('*******************$number*******************');
  }

  Future<List<Task>> fetchItems() async {
    final List<Map<String, dynamic>> maps = await database.query('tasks');
    if (maps.length > 0) {
      return maps.map((element) => Task.fromDb(element)).toList();
    }
    return [];
  }

  Future<List<Task>> newFetchItems() async {
    final List<Map<String, dynamic>> maps = await database.rawQuery("""
        SELECT * FROM tasks WHERE isArchived = 0 ORDER BY isDone ASC
      """);
    if (maps.length > 0) {
      return maps.map((element) => Task.fromDb(element)).toList();
    }
    return [];
  }

  Future<void> deleteItem(int id) async {
    int number = await database.delete(
      'tasks',
      whereArgs: [id],
      where: 'id = ?',
    );
    print('*******************$number*******************');
  }

  Future<void> updateItem(Task task) async {
    int number = await database.update(
      'tasks',
      task.toMapForDb(),
      whereArgs: [task.id],
      where: "id = ?",
    );

    print('*******************$number*******************');
  }
}
