import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/widgets/app_drawer.dart';
import 'package:todoey_flutter/widgets/archived_tasks_list.dart';
import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/enums/drawer_state.dart';

class ArchivedTasksScreen extends StatefulWidget {
  static const String screenName = 'archive-screen';

  @override
  _ArchivedTasksScreenState createState() => _ArchivedTasksScreenState();
}

class _ArchivedTasksScreenState extends State<ArchivedTasksScreen> {
  final DrawerState _archivedTasksScreenDrawerState = DrawerState.Archived;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        title: const Text('Archived Tasks'),
      ),
      body: Container(
        child: Consumer<Tasks>(
          builder: (context, tasks, child) => tasks.archivedTasks.isEmpty
              ? _centeredTextWidget()
              : ArchivedTasksList(),
        ),
      ),
      drawer: AppDrawer(_archivedTasksScreenDrawerState),
    );
  }

  Widget _centeredTextWidget() {
    return const Center(
      child: Text(
        'No Archived Tasks Yet!',
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
  }
}
