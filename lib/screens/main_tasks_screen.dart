import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import 'package:todoey_flutter/widgets/main_tasks_list.dart';
import 'package:todoey_flutter/screens/add_task_screen.dart';
import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/widgets/app_drawer.dart';

import 'package:todoey_flutter/enums/drawer_state.dart';

class MainTasksScreen extends StatefulWidget {
  @override
  _MainTasksScreenState createState() => _MainTasksScreenState();
}

class _MainTasksScreenState extends State<MainTasksScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController _controller = ScrollController();
  final DrawerState _tasksScreenDrawerState = DrawerState.Main;
  bool _isVisible = false;

  @override
  initState() {
    super.initState();
    _isVisible = true;
    _controller.addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(_tasksScreenDrawerState),
      body: Container(
        color: Colors.lightBlueAccent,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _drawerIconWidget(),
            _appTitleAndInfo(),
            _mainList(),
          ],
        ),
      ),
      floatingActionButton: Visibility(
        visible: _isVisible,
        child: FloatingActionButton(
          child: const Icon(Icons.add, color: Colors.white),
          backgroundColor: Colors.lightBlueAccent,
          onPressed: () => showModalBottomSheet(
            context: context,
            builder: (ctx) => AddTaskScreen(),
          ),
        ),
      ),
    );
  }

  Widget _drawerIconWidget() {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.only(top: 70, left: 40),
        child: const CircleAvatar(
          backgroundColor: Colors.white,
          radius: 30,
          child: const Icon(
            Icons.list,
            color: Colors.lightBlueAccent,
            size: 30,
          ),
        ),
      ),
      onTap: () => _scaffoldKey.currentState.openDrawer(),
    );
  }

  Widget _appTitleAndInfo() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 40),
      child: Column(
        children: <Widget>[
          _appTitleTextWidget(),
          Consumer<Tasks>(
            builder: (context, tasks, child) => Row(
              children: <Widget>[
                _tasksStatus('${tasks.tasksLength} Tasks'),
                _tasksStatus('${tasks.doneTasks} Done'),
                _tasksStatus('${tasks.remainTasks} Remained'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _appTitleTextWidget() {
    return Container(
      alignment: Alignment.centerLeft,
      child: const Text(
        'Todoey',
        style: const TextStyle(
          color: Colors.white,
          fontSize: 50,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  Widget _tasksStatus(String text) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.only(left: 10),
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 18,
        ),
      ),
    );
  }

  Widget _mainList() {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.only(top: 30),
        decoration: const BoxDecoration(
          borderRadius: const BorderRadius.vertical(top: Radius.circular(20)),
          color: Colors.white,
        ),
        child: Consumer<Tasks>(
          builder: (context, tasks, child) => tasks.tasksData.isEmpty
              ? _noTasksTextWidget()
              : MainTasksList(_controller, _scaffoldKey),
        ),
      ),
    );
  }

  Widget _noTasksTextWidget() {
    return Center(
      child: const Text(
        'No Tasks Yet!',
        style: const TextStyle(
          color: Colors.lightBlueAccent,
          fontSize: 40,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  void _scrollListener() {
    if (_controller.position.atEdge &&
        (_controller.position.pixels == _controller.position.maxScrollExtent)) {
      setState(() => _isVisible = false);
    } else {
      setState(() => _isVisible = true);
    }
  }
}
