import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart' as intl;

import 'package:todoey_flutter/providers/tasks.dart';
import 'package:todoey_flutter/widgets/show_add_alert.dart';

class AddTaskScreen extends StatefulWidget {
  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final TextEditingController _taskTitleController = TextEditingController();
  final TextEditingController _taskSubTitleController = TextEditingController();
  String _newTaskTitle = '';
  String _newTaskSubTitle = '';
  String _taskTitleErrorText;
  String _taskSubTitleErrorText;

  @override
  void initState() {
    super.initState();
    _taskTitleController.addListener(() {
      _newTaskTitle = _taskTitleController.text;
    });
    _taskSubTitleController.addListener(() {
      _newTaskSubTitle = _taskSubTitleController.text;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _taskTitleController.dispose();
    _taskSubTitleController.dispose();
  }

  void _submitEnteredInformation(Tasks tasks) {
    if ((_newTaskTitle != '' &&
            !tasks.isDateIncluded &&
            !tasks.isSubTitleIncluded) ||
        (_newTaskTitle != '' &&
            tasks.isDateIncluded &&
            tasks.selectedDate != null &&
            !tasks.isSubTitleIncluded &&
            _newTaskSubTitle == '') ||
        (_newTaskTitle != '' &&
            tasks.isSubTitleIncluded &&
            _newTaskSubTitle != '' &&
            !tasks.isDateIncluded &&
            tasks.selectedDate == null) ||
        (_newTaskTitle != '' &&
            tasks.isSubTitleIncluded &&
            _newTaskSubTitle != '' &&
            tasks.isDateIncluded &&
            tasks.selectedDate != null)) {
      if (tasks.tasksData.any((element) => element.title == _newTaskTitle)) {
        showDialog(
          context: context,
          builder: (ctx) => ShowAddAlert(_newTaskTitle, _newTaskSubTitle),
        );
      } else {
        tasks.addTask(_newTaskTitle, _newTaskSubTitle);
        Navigator.of(context).pop();
        _setStatesOff(context);
      }
    } else {
      if (_newTaskTitle == '') {
        setState(() => _taskTitleErrorText = 'Task should have a title!');
      } else if ((tasks.isSubTitleIncluded && _newTaskSubTitle == '') &&
          (tasks.isDateIncluded && tasks.selectedDate == null)) {
        setState(() {
          _taskTitleErrorText = 'Date must get picked!';
          _taskSubTitleErrorText = 'You should enter subtitle';
        });
      } else if (tasks.isSubTitleIncluded && _newTaskSubTitle == '') {
        setState(() {
          _taskSubTitleErrorText = 'You should enter subtitle';
          _taskTitleErrorText = null;
        });
      } else if (tasks.isDateIncluded && tasks.selectedDate == null) {
        setState(() {
          _taskTitleErrorText = 'Date must get picked!';
          _taskSubTitleErrorText = null;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<Tasks>(
      builder: (context, tasks, child) => WillPopScope(
        onWillPop: () async {
          _setStatesOff(context);
          return true;
        },
        child: Container(
          color: const Color(0xff757575),
          child: SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.vertical(
                  top: const Radius.circular(10),
                ),
              ),
              padding: EdgeInsets.only(
                top: 20,
                bottom: MediaQuery.of(context).viewInsets.bottom + 20,
              ),
              child: Column(
                children: <Widget>[
                  _addTaskTextWidget(),
                  _textFieldWidget(
                    null,
                    'Enter Task Title',
                    _taskTitleErrorText,
                    _taskTitleController,
                    tasks,
                  ),
                  tasks.isSubTitleIncluded
                      ? _textFieldWidget(
                          const EdgeInsets.symmetric(vertical: 30),
                          'Enter Task Description',
                          _taskSubTitleErrorText,
                          _taskSubTitleController,
                          tasks,
                        )
                      : Container(),
                  const SizedBox(height: 40),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _flatButtonWidget(tasks),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _includeDataWidget(
                            'Include Task Description',
                            tasks.isSubTitleIncluded,
                            (newValue) =>
                                tasks.changeIsSubtitleIncluded(newValue),
                          ),
                          _includeDataWidget(
                            'Include Task Deadline',
                            tasks.isDateIncluded,
                            (newValue) => tasks.changeIsDateIncluded(newValue),
                          ),
                          tasks.isDateIncluded
                              ? _dateWidget(tasks)
                              : Container(),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _addTaskTextWidget() {
    return Container(
      child: const Text(
        'Add Task',
        style: const TextStyle(
          color: Colors.lightBlueAccent,
          fontSize: 24,
        ),
      ),
    );
  }

  Widget _textFieldWidget(EdgeInsetsGeometry margin, String hintText,
      String errorText, TextEditingController controller, Tasks tasks) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      margin: margin,
      child: TextField(
        decoration: InputDecoration(
          hintText: hintText,
          errorText: errorText,
        ),
//        textDirection: TextDirection.rtl,
        autofocus: true,
        controller: controller,
        onSubmitted: (_) => _submitEnteredInformation(tasks),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _flatButtonWidget(Tasks tasks) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: FlatButton(
          color: Colors.lightBlueAccent,
          child: const Text(
            'Add',
            style: const TextStyle(color: Colors.white),
          ),
          onPressed: () => _submitEnteredInformation(tasks),
        ),
      ),
    );
  }

  Widget _includeDataWidget(String text, bool value, Function onChanged) {
    return Row(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Checkbox(
          value: value,
          onChanged: onChanged,
          activeColor: Colors.lightBlueAccent,
        ),
      ],
    );
  }

  Widget _dateWidget(Tasks tasks) {
    return Row(
      children: <Widget>[
        InkWell(
          child: const Text(
            'Choose Date',
            style: const TextStyle(
              color: Colors.lightBlueAccent,
            ),
          ),
          onTap: () => tasks.selectDate(context),
        ),
        const SizedBox(width: 10),
        Text(
          tasks.selectedDate == null
              ? 'No Date Chosen!'
              : intl.DateFormat('dd/MM/yyyy').format(tasks.selectedDate),
          style: const TextStyle(color: Colors.grey),
        ),
        const SizedBox(width: 10),
      ],
    );
  }

  void _setStatesOff(BuildContext context) {
    Provider.of<Tasks>(context, listen: false).changeIsDateIncluded(false);
    Provider.of<Tasks>(context, listen: false).changeIsSubtitleIncluded(false);
    Provider.of<Tasks>(context, listen: false).changeSelectedDate(null);
  }
}
