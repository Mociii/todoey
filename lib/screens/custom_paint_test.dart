import 'dart:math';
//import 'dart:ui';

import 'package:flutter/material.dart';

class CustomPaintTest extends StatefulWidget {
  @override
  _CustomPaintTestState createState() => _CustomPaintTestState();
}

class _CustomPaintTestState extends State<CustomPaintTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Custom Paint Test')),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.red.shade200,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.red.shade50,
          alignment: Alignment.center,
          child: Container(
            height: double.infinity,
            width: double.infinity,
            color: Colors.yellow,
            child: CustomPaint(
//            foregroundPainter: MyCustomPainter(),
              painter: MyCustomPainter(),
            ),
          ),
        ),
      ),
    );
  }
}

class MyCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    print("***********************$size*******************************");
    Paint paint = Paint()
      ..color = Colors.blue
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 8;

    double degreeToRadian(double deg) => deg * (pi / 180);

    final Path path = Path();
    path.arcTo(
      Rect.fromLTWH(
        size.width / 2,
        size.height / 2,
        size.width / 4,
        size.height / 4,
      ),
      degreeToRadian(0),
      degreeToRadian(90),
      true,
    );
    canvas.drawPath(path, paint);
    canvas.drawRect(
        Rect.fromLTWH(
          size.width / 2,
          size.height / 2,
          size.width / 4,
          size.height / 4,
        ),
        paint);
    path.close();
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
